const path = require('path')
module.exports = {
    outputDir: path.resolve(__dirname, '../server/public'),
    devServer: {
        open: process.platform === 'darwin',
        host: '0.0.0.0',
        port: 8085, // CHANGE YOUR PORT HERE!
        https: true,
        hotOnly: false,
        proxy: {
            '/api/v1' : {
                target: 'http://localhost:5000'
            }

        }
    },
}