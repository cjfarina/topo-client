// Import the styles directly. (Or you could add them via script tags.)
import './custom.scss'

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import VueConfetti from "vue-confetti";
import VTour from './components/tour/VTour'
import VStep from './components/tour/VStep'
import VueCarousel from 'vue-carousel';
import VueSocialSharing from 'vue-social-sharing'

const VueTour = {
  install (Vue, options) {
    Vue.component(VTour.name, VTour)
    Vue.component(VStep.name, VStep)

    // Object containing Tour objects (see VTour.vue) where the tour name is used as key
    Vue.prototype.$tours = {}
  }
}

Vue.use(VueTour)
Vue.use(VueCarousel);

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VueTour)
}

Vue.use(VueConfetti)
Vue.use(VueSocialSharing)

const base = axios.create({
  baseURL: "/api/v1"
});

Vue.prototype.$http = base;
Vue.config.productionTip = false;

const token = localStorage.getItem('jwt')

if (token) {
  base.defaults.headers.common['Authorization'] = token
}

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
