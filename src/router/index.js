import Vue from "vue";
import VueRouter from "vue-router";
import VueAnalytics from 'vue-ua'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "index",
    component: () => import("../components/index.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

Vue.use(VueAnalytics, {
  // [Required] The name of your app as specified in Google Analytics.
  appName: "thesinglepixelowner",
  // [Required] The version of your app.
  appVersion: "1.0",
  // [Required] Your Google Analytics tracking ID.
  trackingId: "UA-3619867-3",
  // If you're using vue-router, pass the router instance here.
  vueRouter: router,

});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwt") == null) {
      next({
        path: "/",
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
